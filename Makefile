clean:
	@find . -name "*.pyc" -delete

setup: deps settings syncdb

syncdb:
	$(VIRTUAL_ENV)/bin/python manage.py syncdb --noinput
	$(VIRTUAL_ENV)/bin/python manage.py migrate

deps:
	$(VIRTUAL_ENV)/bin/pip install -r dev_requirements.txt

settings:
	cp xsdes/local_settings.example.py xsdes/local_settings.py

run:
	$(VIRTUAL_ENV)/bin/python manage.py runserver 0.0.0.0:8000

update:
	@git push heroku master

remote_syncdb:
	@heroku run python manage.py syncdb --noinput
	@heroku run python manage.py migrate

static:
	@heroku run python manage.py collectstatic

deploy: update remote_syncdb static

loaddemo: syncdb
	$(VIRTUAL_ENV)/bin/python manage.py loaddata demo/demo_data.json
	@cp -R demo/demo_media media

test:
	coverage erase
	coverage run --include="xsdes/*" manage.py test --with-xunit --settings=xsdes.settings
	coverage xml

help:
	@grep '^[^#[:space:]].*:' Makefile | awk -F ":" '{print $$1}'
