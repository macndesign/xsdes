# coding: utf-8
from __future__ import unicode_literals, absolute_import
from django.conf import settings
from django.contrib import messages
from django.core.mail.message import EmailMessage
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.template.loader import render_to_string
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView
from django.utils.translation import ugettext as _
from django.shortcuts import redirect, render_to_response, get_object_or_404
from django.template import RequestContext
from django.contrib.auth.models import User
from .forms import ContactForm
from .models import Profile
from .forms import ProfileForm, UserForm, UserUpdateForm


class HomeTemplateView(TemplateView):
    template_name = 'base.html'


def logged_in(request):
    return redirect(request.user.get_profile().get_absolute_url())


def register(request):
    userform = UserForm(request.POST or None)
    userprofileform = ProfileForm(request.POST or None)

    if userform.is_valid() and userprofileform.is_valid():
        user = userform.save()
        userprofile = userprofileform.save(commit=False)
        userprofile.user = user
        userprofile.save()
        return redirect(userprofile.get_absolute_url())

    return render_to_response(
        'core/register.html',
        dict(userform=userform, userprofileform=userprofileform),
        context_instance=RequestContext(request),
    )


def register_update(request):
    user = get_object_or_404(User, pk=request.user.pk)

    if request.method == 'POST':
        userform = UserUpdateForm(request.POST, instance=user)
        userprofileform = ProfileForm(request.POST, instance=user.get_profile())

        if userform.is_valid() and userprofileform.is_valid():
            user = userform.save()
            userprofile = userprofileform.save(commit=False)
            userprofile.user = user
            userprofile.save()
            return redirect(userprofile.get_absolute_url())

    else:
        userform = UserUpdateForm(instance=user)
        userprofileform = ProfileForm(instance=user.get_profile())

    return render_to_response(
        'core/register.html',
        dict(userform=userform, userprofileform=userprofileform),
        context_instance=RequestContext(request),
    )


class ProfileDetailView(DetailView):
    model = Profile


class ContactView(FormView):
    template_name = 'core/contact.html'
    form_class = ContactForm

    def get_success_url(self):
        return reverse_lazy('core:contact')

    def form_invalid(self, form):
        messages.add_message(self.request, messages.ERROR,
                             _(u'O formulário está inválido. Todos os campos são requeridos.'))
        return self.render_to_response(self.get_context_data(form=form))

    def form_valid(self, form):
        """
        This method is called when valid form data has been POSTed.
        It should return an HttpResponse.
        """

        name = form.cleaned_data['name']
        email = form.cleaned_data['email']
        phone = form.cleaned_data['phone']
        subject = form.cleaned_data['subject']
        message_part = form.cleaned_data['message']
        message = render_to_string(
            'snippets/email.html', {
                'name': name,
                'email': email,
                'phone': phone,
                'subject': subject,
                'message': message_part,
            }
        )

        msg = EmailMessage(subject=subject, body=message, from_email=email, to=[settings.DEFAULT_TO_EMAIL])
        msg.content_subtype = 'html'

        try:
            msg.send()
            messages.add_message(self.request, messages.SUCCESS, _('Mensagem enviada com sucesso!'))
        except Exception, e:
            messages.add_message(self.request, messages.ERROR, e)

        return HttpResponseRedirect(reverse_lazy('core:contact'))


class RobotsTemplateView(TemplateView):
    template_name = 'robots.txt'

    def render_to_response(self, context, **response_kwargs):
        return self.response_class(
            request=self.request,
            template=self.get_template_names(),
            context=context,
            mimetype='test/plain',
            **response_kwargs
        )


class HumansTemplateView(TemplateView):
    template_name = 'humans.txt'

    def render_to_response(self, context, **response_kwargs):
        return self.response_class(
            request=self.request,
            template=self.get_template_names(),
            context=context,
            mimetype='test/plain',
            **response_kwargs
        )


class CrossDomainTemplateView(TemplateView):
    template_name = 'crossdomain.xml'

    def render_to_response(self, context, **response_kwargs):
        return self.response_class(
            request=self.request,
            template=self.get_template_names(),
            context=context,
            mimetype='application/xml',
            **response_kwargs
        )
