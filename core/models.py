# coding: utf-8
from __future__ import unicode_literals, absolute_import
from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify


class Profile(models.Model):
    user = models.OneToOneField(User)
    slug = models.SlugField(max_length=100, unique=True, editable=False)
    site = models.URLField(blank=True, null=True)
    twitter = models.CharField(max_length=100, blank=True, null=True)
    facebook = models.CharField(max_length=100, blank=True, null=True)
    linkedin = models.CharField(max_length=100, blank=True, null=True)
    github = models.URLField(blank=True, null=True)
    bitbucket = models.URLField(blank=True, null=True)
    cv = models.TextField(blank=True, null=True)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.pk:
            self.slug = slugify(self.user.username)
        super(Profile, self).save(force_insert, force_update, using)

    class Meta:
        verbose_name = 'Perfil'
        verbose_name_plural = 'Perfis'

    def get_absolute_url(self):
        return '/%s/' % self.slug

    def __unicode__(self):
        return self.user.username
