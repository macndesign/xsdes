# coding: utf-8
from __future__ import unicode_literals, absolute_import
import re
from django import forms
from django.utils.translation import ugettext as _
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.models import User
from django.forms.fields import Field
from django.core.validators import EMPTY_VALUES
from django.forms import ValidationError
from .models import Profile

try:
    from django.utils.encoding import smart_text
except ImportError:
    from django.utils.encoding import smart_unicode as smart_text

phone_digits_re = re.compile(r'^(\d{2})[-\.]?(\d{4,5})[-\.]?(\d{4})$')


class BRPhoneNumberField(Field):
    default_error_messages = {
        'invalid': _(u'Os números de telefone deve estar em um dos seguintes '
                     'formatos: 99-9999-9999 ou 99-99999-9999, pode ser escrito sem os hífens.'),
    }

    def clean(self, value):
        super(BRPhoneNumberField, self).clean(value)
        if value in EMPTY_VALUES:
            return ''
        value = re.sub('(\(|\)|\s+)', '', smart_text(value))
        m = phone_digits_re.search(value)
        if m:
            return '%s-%s-%s' % (m.group(1), m.group(2), m.group(3))
        raise ValidationError(self.error_messages['invalid'])


class ContactForm(forms.Form):
    name = forms.CharField(label=_('Nome'), required=True)
    email = forms.EmailField(label=_('Email'), required=True)
    phone = BRPhoneNumberField(label=_('Telefone'), required=True)
    subject = forms.CharField(label=_('Assunto'), required=True)
    message = forms.CharField(label=_('Mensagem'), widget=forms.Textarea, required=True)


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        exclude = ('user',)


class UserForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        self.fields.keyOrder = ['username', 'password1', 'password2', 'first_name', 'last_name', 'email']

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')


class UserUpdateForm(UserChangeForm):
    def __init__(self, *args, **kwargs):
        super(UserUpdateForm, self).__init__(*args, **kwargs)
        self.fields.keyOrder = ['username', 'first_name', 'last_name', 'email']

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')
