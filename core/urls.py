# coding: utf-8
from __future__ import unicode_literals, absolute_import
from core.views import HomeTemplateView, CrossDomainTemplateView, RobotsTemplateView, HumansTemplateView, ContactView, ProfileDetailView
from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^$', HomeTemplateView.as_view(), name='home'),
    url(r'^contato/$',  ContactView.as_view(), name='contact'),

    # User e Profile
    url(r'^autenticar/$', 'django.contrib.auth.views.login', name='login'),
    url(r'^autenticado/$', 'core.views.logged_in', name='logged_in'),
    url(r'^sair/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='logout'),
    url(r'^cadastro/$', 'core.views.register', name='register'),
    url(r'^cadastro/editar/$', 'core.views.register_update', name='register_update'),
    url(r'^senha/alterar/$', 'django.contrib.auth.views.password_change', {'post_change_redirect': '/'}, name='password_change'),

    # Redefinir senha
    url(r'^password_reset/$', 'django.contrib.auth.views.password_reset', name='password_reset'),
    url(r'^password_reset/done/$', 'django.contrib.auth.views.password_reset_done', name='password_reset_done'),
    url(r'^reset/(?P<uidb36>[0-9A-Za-z]{1,13})-(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        'django.contrib.auth.views.password_reset_confirm',
        name='password_reset_confirm'),
    url(r'^reset/done/$', 'django.contrib.auth.views.password_reset_complete', name='password_reset_complete'),

    # SEO
    url(r'^robots\.txt$', RobotsTemplateView.as_view(), name='robots'),
    url(r'^humans\.txt$', HumansTemplateView.as_view(), name='robots'),
    url(r'^crossdomain\.xml$', CrossDomainTemplateView.as_view(), name='crossdomain'),

    # A ultima url deve ser a do profile
    url(r'^(?P<slug>[-\w]+)/$', ProfileDetailView.as_view(), name="profile_detail"),
)
