# coding: utf-8
from __future__ import unicode_literals, absolute_import
from django.views.generic import DetailView, ListView
from .models import Postagem


class BlogListView(ListView):
    queryset = Postagem.publicados.all()
    paginate_by = 3


class BlogDetailView(DetailView):
    model = Postagem
