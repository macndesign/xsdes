# coding: utf-8
from __future__ import unicode_literals, absolute_import
from django.conf.urls import patterns, url
from .views import BlogListView, BlogDetailView

urlpatterns = patterns('',
                       url(r'^$', BlogListView.as_view(), name='list'),
                       url(r'^(?P<slug>[-\w]+)/$', BlogDetailView.as_view(), name='detail'))
