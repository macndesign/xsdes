# coding: utf-8
from __future__ import unicode_literals, absolute_import
from django.contrib import admin
from .models import Postagem


class PostagemAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}


admin.site.register(Postagem, PostagemAdmin)
